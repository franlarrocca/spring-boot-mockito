# Spring boot Mockito

Proyecto a modo de ejemplo sobre como implementar el framework Mockito en pruebas unitarias. Aplicando conceptos como STUB y MOCK.
Además contiene tres herramientas útiles como Swagger (para ver la documentación del servicio REST), Jacoco y SonarQube (para cobertura de código, code smells y demás).

# Requisitos
- Java 11
- Maven
- SonarQube (Opcional)

## ¿Cómo utilizar Swagger?

Una vez levantada la aplicación ingresar a:
 
[http://localhost:8181/v2/api-docs](http://localhost:8181/v2/api-docs) (para obtener el JSON de la documentación).

[http://localhost:8181/swagger-ui/](http://localhost:8181/swagger-ui/) (para acceder a la interfaz gráfica que nos provee Swagger).

## ¿Cómo utilizar SonarQube?

Una vez descargado e instalado SonarQube de manera local debemos ejecutar el archivo **StartSonar.bat** desde la línea de comandos.

Para crear el proyecto ingresamos a la ruta [http://localhost:9000](http://localhost:9000) en la sección de **projects > create project > manually**

En **project display name** colocamos: {groupId}:{artifactId}

En **display name** colocamos el nombre con el cual queremos ver nuestro proyecto en SonarQube.
Generamos el **token** y seleccionamos **Maven** como tecnología de construcción.

Luego ubicándonos en la carpeta raíz del proyecto ejecutar:

```bash
mvn clean install
mvn sonar:sonar -Dsonar.projectKey={groupId}:{artifactId} -Dsonar.host.url=http://localhost:9000 -Dsonar.login={token}
```

[Documentación Sonar para Maven](https://docs.sonarqube.org/latest/analysis/scan/sonarscanner-for-maven/)

## ¿Cómo utilizar Jacoco?

Ubicándonos en la carpeta raíz del proyecto ejecutar:

```bash
mvn package
```

Luego acceder a {rutaDelProyecto}**/target/site/jacoco/index.html**
