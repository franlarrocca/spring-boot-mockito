package com.fl.springbootmockito.bussines.service;

import com.fl.springbootmockito.domain.Cuenta;
import java.math.BigDecimal;
import java.util.List;

public interface CuentaService {

    Cuenta buscarPorId(long id);

    BigDecimal revisarSaldo(long id);

    void transferir(long idCuentaOrigen, long idCuentaDestino, BigDecimal monto, long bancoId);

    List<Cuenta> listarTodas();

    Cuenta guardar(Cuenta cuenta);
}
