package com.fl.springbootmockito.bussines.service.impl;

import com.fl.springbootmockito.bussines.service.BancoService;
import com.fl.springbootmockito.domain.Banco;
import com.fl.springbootmockito.repository.BancoRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class BancoServiceImpl implements BancoService {

    private final BancoRepository bancoRepository;

    public BancoServiceImpl(BancoRepository bancoRepository) {
        this.bancoRepository = bancoRepository;
    }

    @Override
    public int revisarTotalTransferencias(Long id) {
        Banco banco = bancoRepository.findById(id).orElseThrow();
        return banco.getTotalTransferencias();
    }

}
