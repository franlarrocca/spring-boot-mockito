package com.fl.springbootmockito.bussines.service;

public interface BancoService {

    int revisarTotalTransferencias(Long bancoId);
}
