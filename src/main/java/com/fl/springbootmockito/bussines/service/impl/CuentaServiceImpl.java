package com.fl.springbootmockito.bussines.service.impl;

import com.fl.springbootmockito.bussines.service.CuentaService;
import com.fl.springbootmockito.domain.Banco;
import com.fl.springbootmockito.domain.Cuenta;
import com.fl.springbootmockito.repository.BancoRepository;
import com.fl.springbootmockito.repository.CuentaRepository;
import java.math.BigDecimal;
import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class CuentaServiceImpl implements CuentaService {

    private final CuentaRepository cuentaRepository;
    private final BancoRepository bancoRepository;

    public CuentaServiceImpl(CuentaRepository cuentaRepository, BancoRepository bancoRepository) {
        this.cuentaRepository = cuentaRepository;
        this.bancoRepository = bancoRepository;
    }

    @Override
    public Cuenta buscarPorId(long id) {
        return cuentaRepository.findById(id).orElseThrow();
    }

    @Override
    public BigDecimal revisarSaldo(long id) {
        Cuenta cuenta = cuentaRepository.findById(id).orElseThrow();
        return cuenta.getSaldo();
    }

    @Override
    public void transferir(long idCuentaOrigen, long idCuentaDestino, BigDecimal monto, long bancoId) {
        debitarACuentaOrigen(idCuentaOrigen, monto);
        acreditarACuentaDestino(idCuentaDestino, monto);
        sumarTransferenciaABanco(bancoId);
    }

    private void acreditarACuentaDestino(long idCuentaDestino, BigDecimal monto) {
        Cuenta cuentaDestino = cuentaRepository.findById(idCuentaDestino).orElseThrow();
        cuentaDestino.credito(monto);
        cuentaRepository.save(cuentaDestino);
    }

    private void debitarACuentaOrigen(long idCuentaOrigen, BigDecimal monto) {
        Cuenta cuentaOrigen = cuentaRepository.findById(idCuentaOrigen).orElseThrow();
        cuentaOrigen.debito(monto);
        cuentaRepository.save(cuentaOrigen);
    }
    
    private void sumarTransferenciaABanco(long bancoId) {
        Banco banco = bancoRepository.findById(bancoId).orElseThrow();
        int totalTransferecias = banco.getTotalTransferencias();
        banco.setTotalTransferencias(++totalTransferecias);
        bancoRepository.save(banco);
    }

    @Override
    public List<Cuenta> listarTodas() {
        return cuentaRepository.findAll();
    }

    @Override
    public Cuenta guardar(Cuenta cuenta) {
        return cuentaRepository.save(cuenta);
    }

}
