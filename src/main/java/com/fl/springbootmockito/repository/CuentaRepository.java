package com.fl.springbootmockito.repository;

import com.fl.springbootmockito.domain.Cuenta;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CuentaRepository extends JpaRepository<Cuenta, Long> {
    Optional<Cuenta> findByPersona(String persona);
}
