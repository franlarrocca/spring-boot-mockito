package com.fl.springbootmockito.repository;

import com.fl.springbootmockito.domain.Banco;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BancoRepository extends JpaRepository<Banco, Long> {
}
