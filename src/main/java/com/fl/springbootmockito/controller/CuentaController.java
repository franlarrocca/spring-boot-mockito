package com.fl.springbootmockito.controller;

import com.fl.springbootmockito.bussines.service.CuentaService;
import com.fl.springbootmockito.domain.Cuenta;
import com.fl.springbootmockito.dto.TransaccionDto;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.HttpStatus.CREATED;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/cuentas")
public class CuentaController {

    private final CuentaService cuentaService;

    public CuentaController(CuentaService cuentaService) {
        this.cuentaService = cuentaService;
    }

    @GetMapping
    @ResponseStatus(OK)
    public List<Cuenta> listar() {
        return cuentaService.listarTodas();
    }

    @PostMapping
    @ResponseStatus(CREATED)
    public Cuenta guardar(@RequestBody Cuenta cuenta) {
        return cuentaService.guardar(cuenta);
    }

    @GetMapping("/{id}")
    @ResponseStatus(OK)
    public Cuenta detalle(@PathVariable Long id) {
        return cuentaService.buscarPorId(id);
    }

    @PostMapping("/transferir")
    public ResponseEntity<Map<String, Object>> transferir(@RequestBody TransaccionDto transacionDto) {
        cuentaService.transferir(transacionDto.getCuentaOrigenId(),
                transacionDto.getCuentaDestinoId(),
                transacionDto.getMonto(),
                transacionDto.getBancoId());

        Map<String, Object> response = armarRespuesta(transacionDto);

        return ResponseEntity.ok(response);
    }

    private Map<String, Object> armarRespuesta(TransaccionDto transacionDto) {
        Map<String, Object> response = new HashMap<>();
        response.put("date", LocalDate.now());
        response.put("status", OK);
        response.put("message", "Transferencia realizada con éxito.");
        response.put("transaccion", transacionDto);
        return response;
    }

}
