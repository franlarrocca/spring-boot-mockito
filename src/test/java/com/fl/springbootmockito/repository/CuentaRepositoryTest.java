package com.fl.springbootmockito.repository;

import com.fl.springbootmockito.domain.Cuenta;
import java.math.BigDecimal;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

@DataJpaTest
public class CuentaRepositoryTest {

    @Autowired
    CuentaRepository cuentaRepository;

    @Test
    void findById() {
        Optional<Cuenta> cuenta = cuentaRepository.findById(1l);
        assertTrue(cuenta.isPresent());
        assertEquals("Francisco", cuenta.get().getPersona());
    }

    @Test
    void findByPersona() {
        Optional<Cuenta> cuenta = cuentaRepository.findByPersona("Pedro");
        assertTrue(cuenta.isPresent());
        assertEquals("Pedro", cuenta.get().getPersona());
    }

    @Test
    void findByPersona_conPersonaInexistente_lanzaExcepcion() {
        Optional<Cuenta> cuenta = cuentaRepository.findByPersona("Nombre inexistente");
        assertThrows(NoSuchElementException.class, () -> {
            cuenta.orElseThrow();
        });
        assertFalse(cuenta.isPresent());
    }

    @Test
    void findAll() {
        List<Cuenta> cuentas = cuentaRepository.findAll();
        assertFalse(cuentas.isEmpty());
    }

    @Test
    void save() {
        Cuenta cuenta = new Cuenta(null, "Pepe", new BigDecimal("3000"));
        Cuenta cuentaPersistida = cuentaRepository.save(cuenta);

        assertEquals(cuenta.getPersona(), cuentaPersistida.getPersona());
        assertEquals(cuenta.getSaldo().toPlainString(), cuentaPersistida.getSaldo().toPlainString());
        assertNotNull(cuentaPersistida.getId());
    }

    @Test
    void update() {
        Cuenta cuenta = new Cuenta(null, "Pepe", new BigDecimal("3000"));
        Cuenta cuentaPersistida = cuentaRepository.save(cuenta);

        cuentaPersistida.setSaldo(new BigDecimal("5000"));
        Cuenta cuentaActualizada = cuentaRepository.save(cuentaPersistida);

        assertEquals(cuentaPersistida.getPersona(), cuentaActualizada.getPersona());
        assertEquals(cuentaPersistida.getId(), cuentaActualizada.getId());
        assertEquals("5000", cuentaActualizada.getSaldo().toPlainString());
    }

    @Test
    void delete() {
        Cuenta cuenta = new Cuenta(null, "Pepe", new BigDecimal("3000"));
        Cuenta cuentaPersistida = cuentaRepository.save(cuenta);
        assertNotNull(cuentaPersistida.getId());

        cuentaRepository.delete(cuenta);

        assertThrows(NoSuchElementException.class, () -> {
            cuentaRepository.findById(cuentaPersistida.getId()).orElseThrow();
        });
    }

}
