package com.fl.springbootmockito.builder;

public abstract class AbstractBuilder<T> {

    protected T instance;

    public T build() {
        return instance;
    }

}
