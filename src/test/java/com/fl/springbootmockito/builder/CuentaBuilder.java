package com.fl.springbootmockito.builder;

import com.fl.springbootmockito.domain.Cuenta;
import java.math.BigDecimal;

public class CuentaBuilder extends AbstractBuilder<Cuenta> {

    public CuentaBuilder() {
        super.instance = new Cuenta();
    }

    public static CuentaBuilder tipica() {
        CuentaBuilder builder = new CuentaBuilder();
        builder.instance.setPersona("John Doe");
        builder.instance.setSaldo(new BigDecimal("1000"));
        return builder;
    }

    public static CuentaBuilder tipica_01() {
        CuentaBuilder builder = new CuentaBuilder();
        builder.instance.setId(1L);
        builder.instance.setPersona("John Doe");
        builder.instance.setSaldo(new BigDecimal("1000"));
        return builder;
    }

    public static CuentaBuilder tipica_02() {
        CuentaBuilder builder = new CuentaBuilder();
        builder.instance.setId(2L);
        builder.instance.setPersona("Jane Doe");
        builder.instance.setSaldo(new BigDecimal("2000"));
        return builder;
    }

    public CuentaBuilder conId(long id) {
        instance.setId(id);
        return this;
    }

    public CuentaBuilder conPersona(String persona) {
        instance.setPersona(persona);
        return this;
    }

    public CuentaBuilder conSaldo(BigDecimal saldo) {
        instance.setSaldo(saldo);
        return this;
    }

}
