package com.fl.springbootmockito.builder;

import com.fl.springbootmockito.domain.Banco;

public class BancoBuilder extends AbstractBuilder<Banco> {

    public BancoBuilder() {
        super.instance = new Banco();
    }

    public static BancoBuilder tipico() {
        BancoBuilder builder = new BancoBuilder();
        builder.instance.setId(1L);
        builder.instance.setNombre("Banco del estado");
        builder.instance.setTotalTransferencias(0);
        return builder;
    }

    public BancoBuilder conId(long id) {
        instance.setId(id);
        return this;
    }

    public BancoBuilder conNombre(String nombre) {
        instance.setNombre(nombre);
        return this;
    }

    public BancoBuilder conTotalTransferencias(int totalTransferencias) {
        instance.setTotalTransferencias(totalTransferencias);
        return this;
    }

}
