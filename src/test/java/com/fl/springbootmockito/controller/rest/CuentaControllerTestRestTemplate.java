package com.fl.springbootmockito.controller.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fl.springbootmockito.builder.CuentaBuilder;
import com.fl.springbootmockito.domain.Cuenta;
import com.fl.springbootmockito.dto.TransaccionDto;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CuentaControllerTestRestTemplate {

    @Autowired
    private TestRestTemplate client;

    private ObjectMapper objectMapper;

    @LocalServerPort
    private int puerto;

    @BeforeEach
    void setUp() {
        objectMapper = new ObjectMapper();
    }

    @Test
    void transferir_conCuentasExistentesEnLaBD_realizaTransaccion() throws JsonProcessingException {
        TransaccionDto dto = new TransaccionDto();
        dto.setMonto(new BigDecimal("100"));
        dto.setCuentaOrigenId(1l);
        dto.setCuentaDestinoId(2l);
        dto.setBancoId(1l);

        ResponseEntity<String> respuesta = client.postForEntity(armarUrl("/api/cuentas/transferir"), dto, String.class);

        String json = respuesta.getBody();
        assertNotNull(json);
        assertEquals(HttpStatus.OK, respuesta.getStatusCode());
        assertEquals(MediaType.APPLICATION_JSON, respuesta.getHeaders().getContentType());
        assertTrue(json.contains("Transferencia realizada con éxito"));

        JsonNode jsonNode = objectMapper.readTree(json);
        assertEquals(LocalDate.now().toString(), jsonNode.path("date").asText());
        assertEquals("Transferencia realizada con éxito.", jsonNode.path("message").asText());
        assertEquals("100", jsonNode.path("transaccion").path("monto").asText());

    }

    @Test
    void detalle_conCuentaExistenteEnLaBD_retornaDetalleDeCuenta() {
        ResponseEntity<Cuenta> respuesta = client.getForEntity(armarUrl("/api/cuentas/1"), Cuenta.class);
        Cuenta cuenta = respuesta.getBody();

        assertNotNull(cuenta);
        assertEquals(HttpStatus.OK, respuesta.getStatusCode());
        assertEquals(MediaType.APPLICATION_JSON, respuesta.getHeaders().getContentType());

        assertEquals(1l, cuenta.getId());
        assertEquals("Francisco", cuenta.getPersona());
        assertEquals("1000", cuenta.getSaldo().toPlainString());
    }

    @Test
    void listar_conCuentasExistentesEnLaBD_retornaListaDeCuentas() {
        ResponseEntity<Cuenta[]> respuesta = client.getForEntity(armarUrl("/api/cuentas"), Cuenta[].class);
        List<Cuenta> cuentas = Arrays.asList(respuesta.getBody());

        assertThat(cuentas).isNotEmpty();
        assertEquals(HttpStatus.OK, respuesta.getStatusCode());
        assertEquals(MediaType.APPLICATION_JSON, respuesta.getHeaders().getContentType());
        assertEquals(1l, cuentas.get(0).getId());

    }

    @Test
    void guardar_conCuentaValida_guardaCuenta() {
        ResponseEntity<Cuenta> respuesta = client.postForEntity(armarUrl("/api/cuentas"), CuentaBuilder.tipica().build(), Cuenta.class);
        Cuenta cuenta = respuesta.getBody();

        assertNotNull(cuenta);
        assertNotNull(cuenta.getId());
        assertEquals(HttpStatus.CREATED, respuesta.getStatusCode());
        assertEquals(MediaType.APPLICATION_JSON, respuesta.getHeaders().getContentType());

    }

    private String armarUrl(String uri) {
        return "http://localhost:" + puerto + uri;
    }

}
