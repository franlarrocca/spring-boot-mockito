package com.fl.springbootmockito.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fl.springbootmockito.builder.CuentaBuilder;
import com.fl.springbootmockito.bussines.service.CuentaService;
import com.fl.springbootmockito.domain.Cuenta;
import com.fl.springbootmockito.dto.TransaccionDto;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import static org.hamcrest.Matchers.hasSize;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@WebMvcTest(CuentaController.class)
public class CuentaControllerTest {

    @Autowired
    MockMvc mvc;

    @MockBean
    private CuentaService cuentaService;

    private ObjectMapper objectMapper;

    @BeforeEach
    public void setUp() {
        objectMapper = new ObjectMapper();
    }

    @Test
    void listar_conCuentasExistentes_retornaListaDeCuentas() throws Exception {
        List<Cuenta> cuentas = Arrays.asList(
                Optional.of(CuentaBuilder.tipica_01().build()).get(),
                Optional.of(CuentaBuilder.tipica_02().build()).get());
        when(cuentaService.listarTodas()).thenReturn(cuentas);

        mvc.perform(MockMvcRequestBuilders
                .get("/api/cuentas/")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].persona").value("John Doe"))
                .andExpect(jsonPath("$[1].persona").value("Jane Doe"))
                .andExpect(jsonPath("$", hasSize(2)));
        verify(cuentaService).listarTodas();
    }

    @Test
    void detalle_conCuentaExistenteEnBD_retornaCuenta() throws Exception {
        //Given ‘dado’: Se especifica el escenario, las precondiciones.
        when(cuentaService.buscarPorId(1l)).thenReturn(Optional.of(CuentaBuilder.tipica_01().build()).get());
        //When ‘cuando’: Las condiciones de las acciones que se van a ejecutar.
        mvc.perform(MockMvcRequestBuilders
                .get("/api/cuentas/1")
                .contentType(MediaType.APPLICATION_JSON))
                //Then ‘entonces’: El resultado esperado, las validaciones a realizar.        
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.persona").value("John Doe"));
        verify(cuentaService).buscarPorId(1l);
    }

    @Test
    void transferir_conCuentasValidasYBancoValidoExistentesEnBD_realizaTransferencia() throws Exception {
        //Given ‘dado’: Se especifica el escenario, las precondiciones.
        TransaccionDto transaccionDto = new TransaccionDto();
        transaccionDto.setCuentaOrigenId(1l);
        transaccionDto.setCuentaDestinoId(2l);
        transaccionDto.setMonto(new BigDecimal("100"));
        transaccionDto.setBancoId(1l);
        //When ‘cuando’: Las condiciones de las acciones que se van a ejecutar.
        mvc.perform(MockMvcRequestBuilders
                .post("/api/cuentas/transferir")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(transaccionDto)))
                //Then ‘entonces’: El resultado esperado, las validaciones a realizar.        
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.message").value("Transferencia realizada con éxito."));
    }

    @Test
    void guardar_conCuentaValida_guardaCuenta() throws Exception {
        Cuenta cuenta = CuentaBuilder.tipica().build();
        when(cuentaService.guardar(any())).thenReturn(cuenta);

        mvc.perform(MockMvcRequestBuilders
                .post("/api/cuentas")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(cuenta)))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));

        verify(cuentaService).guardar(any());
    }

}
