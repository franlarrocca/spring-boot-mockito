package com.fl.springbootmockito.bussines.service;

import com.fl.springbootmockito.builder.BancoBuilder;
import com.fl.springbootmockito.builder.CuentaBuilder;
import com.fl.springbootmockito.domain.Banco;
import com.fl.springbootmockito.domain.Cuenta;
import com.fl.springbootmockito.exception.DineroInsuficienteException;
import com.fl.springbootmockito.repository.BancoRepository;
import com.fl.springbootmockito.repository.CuentaRepository;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import org.assertj.core.api.Assertions;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.Test;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

@SpringBootTest
class CuentaServiceTest {

    @Autowired
    private CuentaService cuentaService;

    //Creación de los mocks:
    @MockBean
    private CuentaRepository cuentaRepository;
    @MockBean
    private BancoRepository bancoRepository;

    @Test
    void transferir_conDosCuentasValidasYBancoValido_realizaTransferencia() {
        // - Stub -
        when(cuentaRepository.findById(1l)).thenReturn(Optional.of(CuentaBuilder.tipica_01().build()));
        when(cuentaRepository.findById(2l)).thenReturn(Optional.of(CuentaBuilder.tipica_02().build()));
        when(bancoRepository.findById(1l)).thenReturn(Optional.of(BancoBuilder.tipico().build()));
        // - Stub -

        BigDecimal saldoOrigen = cuentaService.revisarSaldo(1l);
        BigDecimal saldoDestino = cuentaService.revisarSaldo(2l);
        assertEquals("1000", saldoOrigen.toPlainString());
        assertEquals("2000", saldoDestino.toPlainString());

        cuentaService.transferir(1l, 2l, new BigDecimal("100"), 1l);

        saldoOrigen = cuentaService.revisarSaldo(1l);
        saldoDestino = cuentaService.revisarSaldo(2l);
        assertEquals("900", saldoOrigen.toPlainString());
        assertEquals("2100", saldoDestino.toPlainString());

        // - Verificando invocaciones -
        verify(cuentaRepository, times(3)).findById(1l);
        verify(cuentaRepository, times(3)).findById(2l);
        verify(cuentaRepository, times(2)).save(any(Cuenta.class));
        verify(cuentaRepository, never()).findAll();

        verify(bancoRepository, times(1)).findById(1l);
        verify(bancoRepository, times(1)).save(any(Banco.class));
        // - Verificando invocaciones -

    }

    @Test
    void transferir_montoMayorAlExistenteEnLaCuenta_lanzaExcepcion() {
        when(cuentaRepository.findById(1l)).thenReturn(Optional.of(CuentaBuilder.tipica_01().build()));
        when(cuentaRepository.findById(2l)).thenReturn(Optional.of(CuentaBuilder.tipica_02().build()));
        when(bancoRepository.findById(1l)).thenReturn(Optional.of(BancoBuilder.tipico().build()));

        BigDecimal saldoOrigen = cuentaService.revisarSaldo(1l);
        BigDecimal saldoDestino = cuentaService.revisarSaldo(2l);
        assertEquals("1000", saldoOrigen.toPlainString());
        assertEquals("2000", saldoDestino.toPlainString());

        Assertions.assertThatExceptionOfType(DineroInsuficienteException.class)
                .isThrownBy(() -> cuentaService.transferir(1l, 2l, new BigDecimal("1200"), 1l))
                .withMessage("Dinero insuficiente en la cuenta.");

        saldoOrigen = cuentaService.revisarSaldo(1l);
        saldoDestino = cuentaService.revisarSaldo(2l);
        assertEquals("1000", saldoOrigen.toPlainString());
        assertEquals("2000", saldoDestino.toPlainString());

        verify(cuentaRepository, times(3)).findById(1l);
        verify(cuentaRepository, times(2)).findById(2l);
        verify(cuentaRepository, never()).save(any(Cuenta.class));

        verify(bancoRepository, never()).save(any(Banco.class));

    }

    @Test
    void listarTodas_conCuentasExistentes_retornaListaDeCuentas() {
        when(cuentaService.listarTodas()).thenReturn(Arrays.asList(
                Optional.of(CuentaBuilder.tipica_01().build()).get(),
                Optional.of(CuentaBuilder.tipica_02().build()).get()));

        List<Cuenta> cuentas = cuentaService.listarTodas();

        assertFalse(cuentas.isEmpty());
        assertEquals(2, cuentas.size());
        assertTrue(cuentas.contains(Optional.of(CuentaBuilder.tipica_01().build()).get()));
        assertTrue(cuentas.contains(Optional.of(CuentaBuilder.tipica_02().build()).get()));

        verify(cuentaRepository).findAll();

    }

    @Test
    void listarTodas_sinCuentasExistentes_retornaListaVacia() {
        when(cuentaService.listarTodas()).thenReturn(Collections.EMPTY_LIST);

        List<Cuenta> cuentas = cuentaService.listarTodas();

        assertTrue(cuentas.isEmpty());

        verify(cuentaRepository).findAll();

    }

    @Test
    void guardar_conCuentaValida_guardaCuentaYAsignaId() {
        Cuenta cuentaMock = new Cuenta(null, "Test", new BigDecimal("1000"));
        when(cuentaRepository.save(any())).then(args -> {
            Cuenta mockCuentaParaPersistencia = args.getArgument(0);
            mockCuentaParaPersistencia.setId(3l);
            return mockCuentaParaPersistencia;
        });
        Cuenta cuenta = cuentaService.guardar(cuentaMock);

        assertEquals("Test", cuenta.getPersona());
        assertEquals(3l, cuenta.getId());

        verify(cuentaRepository).save(any());

    }
}
