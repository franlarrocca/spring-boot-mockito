package com.fl.springbootmockito.bussines.service;

import com.fl.springbootmockito.builder.BancoBuilder;
import com.fl.springbootmockito.builder.CuentaBuilder;
import com.fl.springbootmockito.repository.BancoRepository;
import com.fl.springbootmockito.repository.CuentaRepository;
import java.math.BigDecimal;
import java.util.Optional;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

@SpringBootTest
public class BancoServiceTest {

    @Autowired
    private BancoService bancoService;
    @Autowired
    private CuentaService cuentaService;

    //Creación de los mocks:
    @MockBean
    private CuentaRepository cuentaRepository;
    @MockBean
    private BancoRepository bancoRepository;

    @Test
    void revisarTotalTransferencias_conBancoValidoYAlMenosUnaTransferenciaEntreDosCuentas_retornaCantidadTransferencias() {
        // - Stub -
        when(cuentaRepository.findById(1l)).thenReturn(Optional.of(CuentaBuilder.tipica_01().build()));
        when(cuentaRepository.findById(2l)).thenReturn(Optional.of(CuentaBuilder.tipica_02().build()));
        when(bancoRepository.findById(1l)).thenReturn(Optional.of(BancoBuilder.tipico().build()));
        // - Stub -

        cuentaService.transferir(1l, 2l, new BigDecimal("200"), 1l);

        int total = bancoService.revisarTotalTransferencias(1l);
        assertEquals(1, total);

        // - Verificando invocaciones -
        verify(bancoRepository, times(2)).findById(1l);
        // - Verificando invocaciones -
    }
}
